var express = require('express');
var app = express();
var isSecure = false;

app.get('/', function (req, res) {
  res.send('Welcome to dummy ontap!<br />Configure @ /config');
});

app.get('/config', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify({ isSecure: isSecure }));
  console.log('GET ' + '{ isSecure: ' + isSecure + ' }');
});

app.post('/config', function (req, res) {
  isSecure = req.query.isSecure === 'true';
  res.sendStatus(200);
  console.log('POST ' + '{ isSecure: ' + isSecure + ' }');
});

app.listen(3000);

// custom logger
console.logCopy = console.log.bind(console);
console.log = function(data) {
    var timestamp = '[' + new Date().toISOString() + '] ';
    this.logCopy(timestamp, data);
};
